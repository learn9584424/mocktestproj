package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class ControlTest {

    @Test
    public void testDivision() {
        Control control = new Control();
        assertEquals("Division by a non-zero divisor should work", 2, control.getMax(6, 3));
        assertEquals("Division by zero should return Integer.MAX_VALUE", Integer.MAX_VALUE, control.getMax(1, 0));
    }

    @Test
    public void testDivisionByZero() {
        Control control = new Control();
        assertEquals("Division by zero should return Integer.MAX_VALUE", Integer.MAX_VALUE, control.getMax(1, 0));
    }
}
