package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int expected = 9; // since 3 + 6 = 9
        assertEquals("The addition result should be 9", expected, calculator.add());
    }
}

