package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalcmulTest {

    @Test
    public void testMul() {
        Calcmul calculator = new Calcmul();
        int expected = 18; // since 3 * 6 = 18
        assertEquals("The multiplication result should be 18", expected, calculator.mul());
    }
}

