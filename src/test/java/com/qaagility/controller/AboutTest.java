package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {

    @Test
    public void testDesc() {
        About about = new About();
        String expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals("Description should match the expected string", expected, about.desc());
    }
}

